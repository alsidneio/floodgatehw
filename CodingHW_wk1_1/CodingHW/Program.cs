﻿using System;

namespace CodingHW
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            Console.WriteLine("Hello Again");
            Console.WriteLine("I like typing this.");
            Console.WriteLine("This is fun.");
            Console.WriteLine("Yay! Printing.");
            Console.WriteLine("I'd much rather you 'not'.");
        }
    }
}
