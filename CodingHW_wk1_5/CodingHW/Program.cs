using System;

namespace CodingHW
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            Console.Write("How old are you? ");
            var age = Console.ReadLine();

            Console.Write("\nHow tall are you? ");
            var height = Console.ReadLine();

            Console.Write("\nHow much do you weigh? ");
            var weight = Console.ReadLine();

            Console.WriteLine("\nSo, you're " + age + " old, " + height + " tall and " + weight + "lbs heavy.");
        }
    }
}
