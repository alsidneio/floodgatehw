using System;

namespace CodingHW
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            // declare variables
            int cars = 100;
            double spaceInACar = 4.0;
            int drivers = 30;
            int passengers = 90;
            // this is equal to the difference between cars and drivers
            int carsNotDriven = cars - drivers;

            // this is equal to the number of drivers
            int carsDriven = drivers;

            // this is equal to the product of the number of and space in a car
            double carpoolCapacity = spaceInACar * carsDriven;

            // this is equal to the quotient of passengers and carsDriven

            double averagePassengersPerCar = passengers / carsDriven;

            Console.WriteLine("There are " + cars + " cars available.");
            Console.WriteLine("There are only " + drivers + " drivers available.");
            Console.WriteLine("There will be " + carsNotDriven + " empty cars today.");
            Console.WriteLine("We can transport " + carpoolCapacity.ToString("f1") + " people today.");
            Console.WriteLine("We have " + passengers + " to carpool today.");
            Console.WriteLine("We need to put about " + averagePassengersPerCar.ToString("f1") + " in each car.");

        }
    }
}
